class CreateCalendars < ActiveRecord::Migration[6.0]
  def change
    create_table :calendars do |t|
      t.string :name
      t.string :color_ind
      t.string :location

      t.timestamps
    end
  end
end
