# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# calendar_list = [
#     ['Meeting Room 1', 'red', 'Block A #03-10', 'Meeting Room 1 @ Block A'],
#     ['Meeting Room 2', 'blue', 'Block A #03-12', 'Meeting Room 2 @ Block A'],
#     ['Meeting Room 3', 'green', 'Block B #02-04', 'Meeting Room 3 @ Block B'],
#     ['Meeting Room 4', 'yellow', 'Block B #02-06', 'Meeting Room 4 @ Block B']
# ]
# calendar_list.each do |name, color_ind, location, description|
#   Calendar.create( name: name, color_ind: color_ind, location: location, description: description)
# end
#
# bookings = [
#     ['Requirement Gathering for Phase 3', Time.now.getutc + 8.hour, Time.now.getutc + 9.hour, 'Joseph', 1, 'This meeting is for everyone to sync up in prep for phase 3 requirements.'],
#     ['Requirement Gathering for Phase 4', Time.now.getutc + 8.hour, Time.now.getutc + 9.hour, 'Donald', 2, 'This meeting is for everyone to sync up in prep for phase 4 requirements.']
# ]
# bookings.each do |title, start_date, end_date, who, calendars_id, description|
#   Booking.create( title: title, start_date: start_date, end_date: end_date, who: who, calendars_id: calendars_id, description: description)
# end

mr1 = Calendar.create(name: 'Meeting Room 1', color_ind: 'red', location: 'Block A #03-10')
mr2 = Calendar.create(name: 'Meeting Room 2', color_ind: 'blue', location: 'Block A #03-12')
mr3 = Calendar.create(name: 'Meeting Room 3', color_ind: 'green', location: 'Block B #02-04')
mr4 = Calendar.create(name: 'Meeting Room 4', color_ind: 'yellow', location: 'Block B #02-06')
u1 = User.create(name: 'Daniel')
u2 = User.create(name: 'Ronald')
Booking.create!(title: 'Requirement Gathering for Phase 3', start_date: Time.now, end_date: 1.hour.from_now, user_id: u1.id, calendar_id: mr1.id)
Booking.create!(title: 'Requirement Gathering for Phase 4', start_date: Time.now, end_date: 1.hour.from_now, user_id: u2.id, calendar_id: mr2.id)
