# gets the docker image of ruby 2.5 and lets us build on top of that
FROM ruby:2.5.3

# throw errors if Gemfile has been modified since Gemfile.lock
RUN bundle config --global frozen 1

# install rails dependencies
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs libsqlite3-dev

# create a folder /bookingsystem in the docker container and go into that folder
RUN mkdir /bookingsystem
WORKDIR /bookingsystem

# Copy the Gemfile and Gemfile.lock from app root directory into the /bookingsystem/ folder in the docker container
COPY Gemfile /bookingsystem/Gemfile
COPY Gemfile.lock /bookingsystem/Gemfile.lock

# Run bundle install to install gems inside the gemfile
RUN bundle install

# Copy the whole app
COPY . /bookingsystem