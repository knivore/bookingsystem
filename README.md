# README

This README would normally document whatever steps are necessary to get the
application up and running.

* Ruby version
    - 2.5.3

* System dependencies
    - Docker

* Local Docker Deployment Instructions
    - Step 1: Open new terminal, cd docker/development
    - Step 2: run cmd 'docker-compose build'
    - Step 3: run cmd 'docker-compose up'
    - Step 4: Open separate terminal, cd docker/development
    - Step 5: run cmd 'docker-compose run web rake db:create'
    - Step 6: run cmd 'docker-compose run web rake db:migrate'
    - Step 7: run cmd 'docker-compose run web rake db:seed'
