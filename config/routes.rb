Rails.application.routes.draw do
  devise_for :users
  # resources :users
  # resources :calendars
  resources :bookings

  get 'bookings/date/:param_date', to: 'bookings#show_bookings_by_date', as: 'date_booking'

  get 'calendars/index'
  root 'calendars#index'
end
