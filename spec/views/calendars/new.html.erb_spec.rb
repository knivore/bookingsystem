require 'rails_helper'

RSpec.describe "calendars/new", type: :view do
  before(:each) do
    assign(:calendar, Calendar.new(
      :name => "MyString",
      :color_ind => "MyString",
      :location => "MyString"
    ))
  end

  it "renders new calendar form" do
    render

    assert_select "form[action=?][method=?]", calendars_path, "post" do

      assert_select "input[name=?]", "calendar[name]"

      assert_select "input[name=?]", "calendar[color_ind]"

      assert_select "input[name=?]", "calendar[location]"
    end
  end
end
