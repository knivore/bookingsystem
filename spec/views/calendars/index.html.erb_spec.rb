require 'rails_helper'

RSpec.describe "calendars/index", type: :view do
  before(:each) do
    assign(:calendars, [
      Calendar.create!(
        :name => "Name",
        :color_ind => "Color Ind",
        :location => "Location"
      ),
      Calendar.create!(
        :name => "Name",
        :color_ind => "Color Ind",
        :location => "Location"
      )
    ])
  end

  it "renders a list of calendars" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Color Ind".to_s, :count => 2
    assert_select "tr>td", :text => "Location".to_s, :count => 2
  end
end
