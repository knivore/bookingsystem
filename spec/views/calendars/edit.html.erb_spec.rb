require 'rails_helper'

RSpec.describe "calendars/edit", type: :view do
  before(:each) do
    @calendar = assign(:calendar, Calendar.create!(
      :name => "MyString",
      :color_ind => "MyString",
      :location => "MyString"
    ))
  end

  it "renders the edit calendar form" do
    render

    assert_select "form[action=?][method=?]", calendar_path(@calendar), "post" do

      assert_select "input[name=?]", "calendar[name]"

      assert_select "input[name=?]", "calendar[color_ind]"

      assert_select "input[name=?]", "calendar[location]"
    end
  end
end
