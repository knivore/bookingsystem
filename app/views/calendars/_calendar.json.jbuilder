json.extract! calendar, :id, :name, :color_ind, :location, :created_at, :updated_at
json.url calendar_url(calendar, format: :json)
