json.extract! booking, :id, :title, :start_date, :end_date, :user_id, :calendar_id, :created_at, :updated_at
json.url booking_url(booking, format: :json)
