class Booking < ApplicationRecord

  belongs_to :user, optional: true
  belongs_to :calendar, optional: true

  validates :title, :start_date, :end_date, :user_id, :calendar_id, presence: true
  validate :end_after_start

  def start_time
    start_date
  end

  def end_time
    end_date
  end

  private

  def end_after_start
    return if end_date.blank? || start_date.blank?
    return errors.add(:end_date, 'must be after the start date') unless end_date > start_date
  end
end
