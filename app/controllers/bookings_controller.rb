class BookingsController < ApplicationController
  respond_to :html, :json
  before_action :authenticate_user!
  before_action :check_record_exists?, only: [:create, :edit]
  before_action :set_booking, only: [:show, :edit, :update, :destroy]

  # GET /bookings
  # GET /bookings.json
  def index
    @bookings = Booking.all
  end

  # GET /bookings/1
  # GET /bookings/1.json
  def show
  end

  # GET /bookings/new
  def new
    @booking = Booking.new
    @date = Date.parse(params[:date])
  end

  # GET /bookings/date/28012020
  def show_bookings_by_date
    @paramDate = Date.parse(params[:param_date])
    @bookings = Booking.where('start_date::date <= ? AND end_date::date >= ?', @paramDate, @paramDate)

    respond_with(@paramDate, @bookings) do |format|
      format.html { render :index }
    end
  end

  # GET /bookings/1/edit
  def edit
    @paramDate = Date.parse(params[:start_date])

  end

  # POST /bookings
  # POST /bookings.json
  def create
    @booking = Booking.new(booking_params)

    respond_to do |format|
      if @booking.save
        format.html { redirect_to @booking, notice: 'Booking was successfully created.' }
        format.json { render :show, status: :created, location: @booking }
      else
        format.html { render :new }
        format.json { render json: @booking.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bookings/1
  # PATCH/PUT /bookings/1.json
  def update
    respond_to do |format|
      if @booking.update(booking_params)
        format.html { redirect_to @booking, notice: 'Booking was successfully updated.' }
        format.json { render :show, status: :ok, location: @booking }
      else
        format.html { render :edit }
        format.json { render json: @booking.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bookings/1
  # DELETE /bookings/1.json
  def destroy
    @booking.destroy
    respond_to do |format|
      format.html { redirect_to bookings_url, notice: 'Booking was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_booking
    @booking = Booking.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def booking_params
    params.require(:booking).permit(:title, :start_date, :end_date, :user_id, :calendar_id)
  end

  def check_record_exists?
    if Booking.where('calendar_id = ? AND start_date > ? AND end_date < ?', params[:calendar_id], params[:start_date], params[:start_date]).exists?
      render json: { error: 'Room already reserved.' }
    end
  end
end
