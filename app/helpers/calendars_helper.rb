module CalendarsHelper
  def url_for_previous_view
    ->(param, date_range) { link_to raw("&laquo;"), date_booking_path(date_range.first - 1.day, param => date_range.first - 1.day), remote: :true}
  end

  def url_for_next_view
    ->(param, date_range) { link_to raw("&raquo;"), date_booking_path(date_range.last + 1.day, param => date_range.last + 1.day), remote: :true}
  end
end
